* Fastify example *

Clone this repository and cd into the project folder and run

```

npm install
node

```

A server will start listening on localhost:3000. Check out the following paths.

url  |  description   |
|-------|----------------|
|`'/'` | hello world endpoint  |
|`'/documentation/json'` | the json object representing the api  |
|`'/documentation/yaml'` | the yaml object representing the api  |
|`'/documentation'` | the swagger ui  |
