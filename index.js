// Require the framework and instantiate it
const fastify = require('fastify')()

fastify.register(require('fastify-swagger'), {
  exposeRoute: true,
  yaml: true,
  swagger: {
    info: {
      title: 'Test swagger',
      description: 'testing the fastify swagger api',
      version: '0.1.0'
    },
    host: 'localhost',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    securityDefinitions: {
      apiKey: {
        type: 'apiKey',
        name: 'apiKey',
        in: 'header'
      }
    }
  }
})

// Declare a route
fastify.get('/', {
  schema: {
    description: 'get some data',
    tags: [],
    summary: 'hello world',
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          hello: { type: 'string' }
        }
      }
    }
  }
}, function (request, reply) {
  reply.send({
    hello: 'world'
  })
})

fastify.ready(err => {
  if (err) throw err
  fastify.swagger()
})

// Run the server!
fastify.listen(3000, function (err) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
})
